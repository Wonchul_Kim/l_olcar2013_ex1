function sim_out = Ballbot_Simulator(Model_Param,Task,Controller)
%
% The simulator for Rezero is provided as a MATLAB function. The syntax for
% this function is as bellow:
% sim_out = Ballbot_Simulator(Model_Param,Task,Controller)
%
% sim_out, Model_Param, Task, and Controller are structures defined as follow:
%
% sim_out: Output struct
%             .x: state matrix, x(:,t)
%             .t: simulation time vector
%             .u: control input matrix, u(:,t)
%             .e: events vector, e(:,t)
%             .Controller: utilized Controller;
%
% Model_Param: Mechanical parameters of Rezero. 
% 
% Task: 
%             .dt: sampling time period. 
%             .start_x: It is the initial state vector in time = start_time
%             .start_time: It is the starting time for simulation.
%             .goal_pos: It is the 2D position of the ball center with 
%                        respect to the inertial frame at time = goal_time
%             .goal_vel: It is the velocity of the ball center with
%                        respect to the inertial frame at time = goa_time
%             .goal_time: It is the time at which the robot should have 
%                         reached to the goal.
%             .max_iteration: This defines the maximum number of iterations
%                             for ILQG algorithm
%             .random: This is a batch of random sequences which will be
%                      used in Rezero simulator. 
%             .cost: It is a structure which contains the cost function
%                    elements like h(x(T_f)) and l(x(t),u(t)). 
%
% Controller: Controller structure
%             u(x) = theta(t) ' * BaseFnc(x)
%             .theta:  It is the controller parameters 3D-matrix which has 
%                      3 columns associated to each control input. The third
%                      dimension is associated with the time. Each time step
%                      has its specific level in this 3D matrix.
%             .BaseFnc: Handle to base-function function
%             .time: This vector shows the relation between the 2D-matrix in
%                    the third dimension of the *.theta matrix to the time step



addpath([pwd '\functions'])
sim_out = Ballbot_Simulator_protected(Model_Param,Task,Controller);
rmpath([pwd '\functions'])

end
